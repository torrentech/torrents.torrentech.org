# Hello you developer and hopefully electronic music freek!

This is the code repository for [**Torrentech**](https://www.torrentech.org)'s web interface. Communication with [backend](https://gitlab.com/torrentech/hub.torrentech.org) is done through Javascript JSON fetch() requests. Is is written in revolutionary [**Svelte**](https://svelte.dev) Javascript framework for frontends, along with [**Vite**](https://vitejs.dev) Javascript bundler.<br><br>

If you're interested in development, please contact **pyc** at **Matrix** chat network (**@xphorm:matrix.org**). Main Torrentech chat channel is at https://matrix.to/#/#torrentech:matrix.org. Please do read next part, there is some crucial info that could greatly help you kickstarting the local development.

## Technical information

* **Main** branch here is the **production** branch, as usual. **Development (dev)** branch is the one that you clone and one to make merge requests for. Before developing, please visit [Issues](https://gitlab.com/torrentech/torrents.torrentech.org/-/issues) section and see what are the current issues and what is appropriate to work on so you get assigned.<br>
* Remember that you'll need to branch [backend](https://gitlab.com/torrentech/hub.torrentech.org) too, to be able to work on your local version of the site. **npm install** command needs to be issued to install all the needed packages.<br>
* Local website server is launched with "**npm run dev**", executed in the root directory of the project.