import { writable } from 'svelte/store';
//may be needed: writable(false)
export let authenticated = writable();
export let autologinusername = writable();
export let userlevel = writable();
export let tags = writable([]);
export let modalOpened = writable();